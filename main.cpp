#include <iostream>
#include <memory>
#include "PlayerAbstract.h"
#include "Players/DefaultPlayer.h"
#include "Players/RandomPlayer.h"
#include "Players/TitForTat.h"

void multirunner ();
void runner (std::shared_ptr<PlayerAbstract> player1, std::shared_ptr<PlayerAbstract> player2, int iterations, const std::array<std::array<int, 2>,2> & successMatrix);

int main(int argc, char **argv)
{
	std::cout << "Begin" << std::endl;
	multirunner();
	return 0;
}

void multirunner()
{
	std::shared_ptr<PlayerAbstract> player1 = nullptr;
	std::shared_ptr<PlayerAbstract> player2 = nullptr;
	PlayerAbstract::GameInfo gameInfo;
	gameInfo.maxPossibleGames = 10;
	gameInfo.successMatrix = {{{2, -1}, {3, 0}}};

	/// Running multiple games
	{
		player1.reset();
		player1 = std::make_shared<DefaultPlayer>();
		gameInfo.playerName = "Player 1";
		player1->init(gameInfo);
		player2.reset();
		player2 = std::make_shared<RandomPlayer>();
		gameInfo.playerName = "Player 2";
		player2->init(gameInfo);
		runner(player1, player2, 10, gameInfo.successMatrix);
	}

	{
		player1.reset();
		player1 = std::make_shared<RandomPlayer>();
		gameInfo.playerName = "Player 1";
		player1->init(gameInfo);
		player2.reset();
		player2 = std::make_shared<DefaultPlayer>();
		gameInfo.playerName = "Player 2";
		player2->init(gameInfo);
		runner(player1, player2, 100, gameInfo.successMatrix);
	}

	{
		player1.reset();
		player1 = std::make_shared<DefaultPlayer>();
		gameInfo.playerName = "Player 1";
		player1->init(gameInfo);
		player2.reset();
		player2 = std::make_shared<TitForTat>();
		gameInfo.playerName = "Player 2";
		player2->init(gameInfo);
		runner(player1, player2, 100, gameInfo.successMatrix);
	}

	{
		player1.reset();
		player1 = std::make_shared<RandomPlayer>();
		gameInfo.playerName = "Player 1";
		player1->init(gameInfo);
		player2.reset();
		player2 = std::make_shared<TitForTat>();
		gameInfo.playerName = "Player 2";
		player2->init(gameInfo);
		runner(player1, player2, 100, gameInfo.successMatrix);
	}
}


void runner(std::shared_ptr<PlayerAbstract> player1, std::shared_ptr<PlayerAbstract> player2, int iterations, const std::array<std::array<int, 2>,2> & successMatrix)
{
	int scr_p1 = 0, scr_p2 = 0;
	// Make threaded later
	for (int iter = 0 ; iter < iterations ; iter ++)
	{
		// std::cerr << "\n Iteration: " << iter << std::endl;
		bool coopA = player1->step(iter);
		bool coopB = player2->step(iter);
		player1->previousResult(iter, coopB);
		player2->previousResult(iter, coopA);
		// Calculate Score
		scr_p1 += (successMatrix[coopA ? 0 : 1][coopB ? 0 : 1]);
		scr_p2 += (successMatrix[coopB ? 0 : 1][coopA ? 0 : 1]);
		// std::cerr << "P1 : " << "C:" << coopA << " Res: " << successMatrix[coopA ? 0 : 1][coopB ? 0 : 1]
		// << "\nP2 : " << "C:" << coopB << " Res: " << successMatrix[coopB ? 0 : 1][coopA ? 0 : 1] << std::endl;
	}
	std::cout << "\n Result : " << iterations << " iterations\n"
		<< "\t" << "Player 1 (" << player1->behaviouralName() << "): " << scr_p1 << "\n"
		<< "\t" << "Player 2 (" << player2->behaviouralName() << "): " << scr_p2 << std::endl;

}
