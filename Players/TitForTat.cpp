#include "TitForTat.h"
#include <iostream>

TitForTat::TitForTat()
{
}

TitForTat::~TitForTat()
{
}

void TitForTat::init(const GameInfo& gameInfo)
{
	name = gameInfo.playerName;
}

bool TitForTat::step(int)
{
	return reap;
}

void TitForTat::previousResult(int, bool sow)
{
	reap = sow;
}

std::string TitForTat::behaviouralName()
{
	return m_behaviouralName;
}

