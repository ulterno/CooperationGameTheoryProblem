#pragma once

#include "../PlayerAbstract.h"

class RandomPlayer : public PlayerAbstract
{
public:
	RandomPlayer ();
	virtual ~RandomPlayer();

	std::string behaviouralName () override;
	void init(const GameInfo & gameInfo) override;
	bool step(int stepNumber) override;
	void previousResult(int stepNumber, bool opponentCooperation) override;
private:
	std::string name;
	const std::string m_behaviouralName = "Random";
};
