#pragma once

#include "../PlayerAbstract.h"

class DefaultPlayer : public PlayerAbstract
{
public:
	DefaultPlayer ();
	virtual ~DefaultPlayer();

	std::string behaviouralName () override;
	void init(const GameInfo & gameInfo) override;
	bool step(int stepNumber) override;
	void previousResult(int stepNumber, bool opponentCooperation) override;
private:
	std::string name;
	const std::string m_behaviouralName = "Default";
};
