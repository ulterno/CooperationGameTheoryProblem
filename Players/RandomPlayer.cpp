#include "RandomPlayer.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

RandomPlayer::RandomPlayer()
{
	std::srand(std::time(nullptr));
}

RandomPlayer::~RandomPlayer()
{
}

void RandomPlayer::init(const GameInfo& gameInfo)
{
	name = gameInfo.playerName;
}

bool RandomPlayer::step(int)
{
	return (std::rand() % 2) == 0 ? true : false;
}

void RandomPlayer::previousResult(int, bool){}

std::string RandomPlayer::behaviouralName()
{
	return m_behaviouralName;
}

