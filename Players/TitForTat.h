#pragma once

#include "../PlayerAbstract.h"

class TitForTat : public PlayerAbstract
{
public:
	TitForTat ();
	virtual ~TitForTat ();

	std::string behaviouralName () override;
	void init(const GameInfo & gameInfo) override;
	bool step(int stepNumber) override;
	void previousResult(int stepNumber, bool opponentCooperation) override;
private:
	std::string name;
	const std::string m_behaviouralName = "TitForTat";

	bool reap = true;
};
