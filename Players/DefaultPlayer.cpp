#include "DefaultPlayer.h"
#include <iostream>

DefaultPlayer::DefaultPlayer()
{
}

DefaultPlayer::~DefaultPlayer()
{
}

void DefaultPlayer::init(const GameInfo& gameInfo)
{
	name = gameInfo.playerName;
}

bool DefaultPlayer::step(int)
{
	return true;
}

void DefaultPlayer::previousResult(int, bool){}

std::string DefaultPlayer::behaviouralName()
{
	return m_behaviouralName;
}

