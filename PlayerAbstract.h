#pragma once

#include <array>
#include <string>

/// successMatrix => What I (this player) get from this instance
///						|	other cooperates	|	other doesn't cooperate	|
///						-----------------------------------------------------
/// I cooperate			|		x				|	a						|
/// I don't cooperate	|		y				|	b						|
///						-----------------------------------------------------
/// std::array<std::array<int, 2>,2> successMatrix = {{{x, a}, {y, b}}};

class PlayerAbstract
{
public:

	struct GameInfo
	{
		std::array<std::array<int, 2>,2> successMatrix = {{{2, -1}, {3, 0}}};
		int maxPossibleGames;
		std::string playerName; // For debug output
	};

	// PlayerAbstract();
	// virtual ~PlayerAbstract() = 0;

	virtual std::string behaviouralName () = 0;

	virtual void init (const GameInfo & gameInfo) = 0;
	virtual bool step (int stepNumber) = 0;
	virtual void previousResult (int stepNumber, bool opponentCooperation) = 0;
};
